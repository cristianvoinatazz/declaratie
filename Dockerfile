FROM nginx

COPY ./dist/declaratie /usr/share/nginx/html/dist
COPY ./dist/declaratie /var/www/html/dist
COPY ./config /etc/nginx

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]