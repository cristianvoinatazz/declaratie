import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { PageComponent } from './page.component';
import { OptionComponent } from './sections/option.component';
import { TextComponent } from './sections/text.component';
import { InComponent } from './sections/in.component';

import { DataService } from './services/data.service';
import { SignaturePadModule } from 'angular2-signaturepad';

@NgModule({
  imports:      [ BrowserModule, FormsModule, SignaturePadModule ],
  declarations: [ AppComponent, PageComponent, OptionComponent, TextComponent, InComponent ],
  providers:    [ DataService ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
