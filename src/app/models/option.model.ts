export class OptionModel {

  public name: string;
  public active: boolean = false;
  public value: string;
}
