import { Injectable } from "@angular/core";
import { distinctUntilChanged, filter } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';
import { StateModel } from '../models/state.model';

@Injectable({
  providedIn: "root"
})
export class DataService {

  public static readonly KEY_STATE: string = 'state';


  public state: BehaviorSubject<StateModel> = new BehaviorSubject<StateModel>(undefined);
  public get state$(): Observable<StateModel> {
    return this.state.asObservable().pipe(distinctUntilChanged());
  }

  /**
   * LIFECYCLE
   */


  public constructor() {
    //this.state$.pipe(filter((s) => s !== undefined)).subscribe((s) => {});
    this.loadState();
  }

  /**
   * UTILS
   */

  public getState(): StateModel {
    const storeState: string = this.getStringBy(DataService.KEY_STATE, null);
    if (!storeState) {
      return new StateModel();
    }

    return Object.assign(new StateModel(), JSON.parse(storeState));
  }


  private loadState(): void {
    const st: StateModel  = this.getState();

    if (st) {
      this.state.next(st);
    } else {
      this.state.next(null);
    }
  }

  /**
   * STORAGE
   */


  public getStringBy(key: string, defaultValue: string): string {
    if (!key) {
      return defaultValue;
    }

    const item: string = JSON.parse(window.localStorage.getItem(key));
    return item ? item : defaultValue;
  }

  public getBy<T>(key: string, defaultValue: T): T {
    if (!key) {
      return defaultValue;
    }

    const item: any = JSON.parse(window.localStorage.getItem(key));
    return item ? Object.assign(defaultValue ? defaultValue : {}, item) : defaultValue;
  }

  public getArrayBy<T>(key: string, Type: new () => T, defaultEmpty: boolean = true): T[] {
    if (!key && defaultEmpty) {
      return [];
    }
    const items: any = JSON.parse(window.localStorage.getItem(key));
    if (!!items && Array.isArray(items)) {
      return items.map((i) => Object.assign(new Type(), i));
    }

    return [];
  }

  public hasInStore(key: string): boolean {
    return !key ? false : !!window.localStorage.getItem(key);
  }

  public store(key: string, value: any): void {
    if (!key) {
      return;
    }
    window.localStorage.removeItem(key);
    if (!!value) {
      window.localStorage.setItem(key, JSON.stringify(value))
    }
  }

}
