import {
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { OptionModel } from '../models/option.model';


@Component({
  selector: "text",
  templateUrl: "./text.component.html",
  styleUrls: ["./text.component.css"]
})
export class TextComponent {

  @Input() name: string;
  @Input() value: string;
  @Input() label: string;

  @Output() update: EventEmitter<OptionModel> = new EventEmitter<OptionModel>();

  public updateText(e: string): void {
    const option: OptionModel = new OptionModel();
    option.name = this.name;
    option.value = e;
    this.update.emit(option);
  }

}
