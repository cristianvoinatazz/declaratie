import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild
} from "@angular/core";
import { OptionModel } from "../models/option.model";

@Component({
  selector: "noption",
  templateUrl: "./option.component.html",
  styleUrls: ["./option.component.css"]
})
export class OptionComponent {

  @ViewChild("checker", { static: false, read: ElementRef })
  public checker: ElementRef;

  @Input() name: string;
  @Input() selected: boolean = false;

  @Output() update: EventEmitter<OptionModel> = new EventEmitter<OptionModel>();

  public updateState(e: any): void {
    if (this.name) {
      const data: OptionModel = new OptionModel();
      data.name = this.name;
      data.active = this.checker ? this.checker.nativeElement.checked : false;
      this.update.emit(data);
    }
  }
}
