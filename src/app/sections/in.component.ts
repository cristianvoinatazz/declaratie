import {
    Component,
    Input,
} from "@angular/core";

@Component({
    selector: "in",
    templateUrl: "./in.component.html",
    styleUrls: ["./in.component.css"]
})
export class InComponent {

    @Input() name: string;
    @Input() label: string;

}
