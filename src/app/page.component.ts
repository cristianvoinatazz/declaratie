import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { OptionModel } from "./models/option.model";
import { DataService } from './services/data.service';
import { filter } from 'rxjs/operators';
import { StateModel } from './models/state.model';
import { SignaturePad } from 'angular2-signaturepad';


@Component({
  selector: "page",
  templateUrl: "./page.component.html",
  styleUrls: ["./page.component.css"]
})
export class PageComponent implements OnInit, AfterViewInit {

  @ViewChild("signature", { static: false, read: ElementRef })
  public signatureImage: ElementRef;

  @ViewChild(SignaturePad, { static: false }) signaturePad: SignaturePad;

  @Input() name: string;

  public state: StateModel;
  public signature: string;


  private signaturePadOptions: Object = {
    'minWidth': 5,
    'canvasWidth': 300,
    'canvasHeight': 100,
    'penColor': '#003399',
    'velocityFilterWeight': 0.5,
    'dotSize': 0.1
  };

  /**
   * LIFECYCLE
   */

  public constructor(private dataService: DataService) {}

  public ngOnInit(): void {
    this.dataService.state$.pipe(filter((s) => s !== undefined)).subscribe(
        (s) => {
          this.state = s;

          if (!!this.state['interval']) {
            const d: Date = new Date();
            const hours: number = d.getHours();

            this.state['interval'] = (hours-1) + ":00 - " + (hours+2) + ":00";
          }

          if (!this.state['day']) {
            const d: Date = new Date();
            const hours: number = d.getHours();

            this.state['day'] = ("0" + d.getDate()).slice(-2) + "." + ("0" + (d.getMonth() + 1)).slice(-2) + "." + d.getFullYear();
          }

          if (!!this.state['signature']) {
           this.signature = this.state['signature'];
          }
        }
    );
  }

  public ngAfterViewInit(): void {
    if (this.signature) {
      //const signaturePad = new SignaturePad(this.canvas.nativeElement);

    }
  }

  /**
   * ACTIONS
   */

  public updateState(data: OptionModel): void {
    if (data && this.state) {
      this.state[data.name] = data.value;
      this.dataService.store(DataService.KEY_STATE, JSON.stringify(this.state));
    }
  }

  public changeState(data: OptionModel): void {
    if (data && this.state) {
      this.state[data.name] = data.active ? "true" : null;
      this.dataService.store(DataService.KEY_STATE, JSON.stringify(this.state));
    }
  }

  public editState(data: string): void {
    if (this.state) {
      this.state['day'] = data;
      this.dataService.store(DataService.KEY_STATE, JSON.stringify(this.state));
    }
  }

  public signed(d: any): void {
    this.signature = "" + this.signaturePad.toDataURL();
    if (this.state) {
      this.state['signature'] = this.signature;
      this.dataService.store(DataService.KEY_STATE, JSON.stringify(this.state));
    }
  }
}
