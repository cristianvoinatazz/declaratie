#!/bin/sh

echo Building Docker container and push it to the DockerHUB repo
echo .

npm run build

docker build -t "dekv/declaratie" .

docker login --username dekv --password All4containers
docker tag dekv/delaratie dekv/declaratie:latest
docker push dekv/declaratie:latest

#docker image prune -f